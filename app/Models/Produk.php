<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;

    protected $table ='produk';

    protected $fillable =[
        'id_kategori', 'nama_produk', 'keterangan', 'foto', 
    ];

    public function kategori()
    {
        return $this->hasOne(Kategori::class,'id', 'id_kategori');
    }

    public function detail_produk()
    {
        return $this->hasMany(DetailProduk::class, 'id_produk', 'id');
    }

    
}

                      