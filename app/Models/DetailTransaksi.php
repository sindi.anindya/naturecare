<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailTransaksi extends Model
{
    use HasFactory;

    protected $table = 'detail_transaksi';

    protected $fillable =[
       'id_transaksi', 'id_detail_produk', 'jumlah', 'subtotal'];

    public function transaksi()
    {
        return $this->hasOne(Transaksi::class,'id', 'id_transaksi');
    }

    public function produk_detail()
    {
        return $this->hasOne(DetailProduk::class, 'id', 'id_detail_produk');
    }
}
