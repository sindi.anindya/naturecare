<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ukuran extends Model
{
    use HasFactory;

    protected $table = 'ukuran';

    protected $fillable =[
        'ukuran',
        'keterangan'
    ];

    public function produk_detail(){
        return $this->hasMany(DetailProduk::class);
    }
}
