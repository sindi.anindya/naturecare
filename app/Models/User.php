<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $fillable = 
    [
        'id','nama','email','password','no_telepon','role','alamat'
    ];

    public function kasir()
    {
        return $this->hasMany(Transaksi::class, 'id_kasir','id');
    }

    public function pembeli()
    {
        return $this->hasMany(Transaksi::class, 'id_pembeli','id');
    }

}