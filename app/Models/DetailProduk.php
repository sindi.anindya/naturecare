<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailProduk extends Model
{
    use HasFactory;

    protected $table = 'detail_produk';

    protected $fillable = [
        'id_produk', 'id_ukuran', 'stok', 'hpp', 'harga'
    ]; 
    
    public function produk()
    {
        return $this->hasOne(Produk::class,'id', 'id_produk');
    }

    public function ukuran()
    {
        return $this->belongsTo(Ukuran::class, 'id_ukuran');
    }

    public function transaksi_detail()
    {
        return $this->hasMany(DetailTransaksi::class, 'id_detail_produk','id');
    }

}





