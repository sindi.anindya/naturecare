<?php

namespace App\Http\Controllers\Admin;

use App\Models\DetailProduk;
use App\Http\Controllers\Controller;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Produk;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;




class ListprodukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['listproduk'] = Produk::all();
        return view('admin.kategori.listproduk', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $kategori ['kategori'] = Kategori::all();
        $data['detailproduk'] = DetailProduk::all();
        return view('admin.kategori.formlist', $kategori, $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rule = [
            'kategori => required',
            'nama_produk => required',
            'foto => required',
            'keterangan => required',
        ];
        $customMessages = [
            'kategori.required' => 'Field Kategori Wajib Diisi!',
            'nama_produk.required' => 'Field Nama Produk Wajib Diisi',
            'foto' => 'Field Foto Wajib Diisi',
            'keterangan.required' => 'Field Keterangan Wajib Diisi!',
        ];

        $this->validate($request, $rule, $customMessages);
        
        $input = $request->all();
                if ($request->hasFile('foto')) {
                     $foto = $request->file('foto');
                     $foto_ext = $foto->getClientOriginalExtension();
                     $foto_name = Str::random(8);

                     $upload_path = 'assets/uploads/user/karakter';
                     $imagename = $upload_path.'/'.$foto_name.'.'.$foto_ext;
                     $request->file('foto')->move($upload_path,$imagename);

                     $input['foto'] = $imagename;
                }
        $status = Produk::create($input);
        if ($status){
            return redirect('listproduk')->with('success', 'Data berhasil ditambahkan');
        }else{
            return redirect('listproduk/create')->with('error', 'Data gagal ditambahkan');
        }
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $listproduk = Produk::find($id);
        $kategori ['kategori'] = Kategori::all();
        $data['listproduk'] = $listproduk;
        $data['detailproduk'] = DetailProduk::where('id_produk', $id)->get();
        return view('admin.kategori.formlist', $kategori, $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rule = [
            'id_kategori => required',
            'nama_produk => required',
            'foto => required',
            'keterangan => required',
        ];
        $customMessages = [
            'id_kategori.required' => 'Field Id Kategori Wajib Diisi!',
            'nama_produk.required' => 'Field Nama Produk Wajib Diisi',
            'foto.required' => 'Field Foto Wajib Diisi',
            'keterangan.required' => 'Field Keterangan Wajib Diisi!',
        ];

        $this->validate($request, $rule, $customMessages);
        $input = $request->all();

        $listproduk = Produk::find($id);
        $listproduk->id_kategori = $request->id_kategori;
        $listproduk->nama_produk = $request->nama_produk;
        $listproduk->keterangan = $request->keterangan;
        if ($request->hasFile('foto')) {
            File::delete($listproduk->foto);

            $foto = $request->file('foto');
            $foto_ext = $foto->getClientOriginalExtension();
            $foto_name = Str::random(8);

            $upload_path = 'assets/uploads/user/karakter';
            $imagename = $upload_path.'/'.$foto_name.'.'.$foto_ext;
            $request->file('foto')->move($upload_path,$imagename);

        $listproduk['foto'] = $imagename;
    }

    // dd($listproduk);

   
        $status = $listproduk->save();
        if ($status){
            return redirect('listproduk')->with('success', 'Data berhasil diubah');
        }else{
            return redirect('listproduk/formlist')->with('error', 'Data gagal diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $listproduk = Produk::find($id);
        $status = $listproduk->delete();
        
        if ($status) {
            return 1;
        }else{
            return 0;
        
        }
    }
}