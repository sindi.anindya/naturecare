<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Transaksi;
use App\Models\DetailTransaksi;
use App\Models\User;
use App\Models\Produk;
use App\Models\Ukuran;
use App\Models\DetailProduk;
use Illuminate\Http\Request;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = Produk::get();
        $detail = DetailProduk::orderBy('harga', 'ASC')->get();
        $transaksi = Transaksi::orderBy('id', 'DESC')->get();
        $cart = session('cart');
        $data['kasir'] =User::where('role', 1)->get();
        $data['pembeli'] =User::where('role', 2)->get();
        // return dd($cart);
        return view('admin.transaksi.index', $data, ['detail' => $detail,'transaksi' => $transaksi,'produk' => $produk, 'cart' => $cart]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['kasir'] =User::where('role', 1)->get();
        $data['pembeli'] =User::where('role', 2)->get();
        return view('admin.transaksi.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request->all());
        //
        $rule = [
            'id_kasir' => 'required',
            'id_pembeli' => 'required',
            'harga_total' => 'required'
            // 'id_transaksi' => 'required',
            // 'id_detail_produk' => 'required',
            // 'jumlah' => 'required',
            // 'subtotal' => 'required'
        ];

        $customMessages = [
            'id_kasir.required' => 'Field Kasir Wajib Diisi!',
            'id_pembeli.required' => 'Field Pembeli Wajib Diisi!',
            'harga_total.required' => 'Field Harga Total Wajib Diisi!'
            // 'id_transaksi.required' => 'Field Kasir Wajib Diisi!',
            // 'id_detail_produk.required' => 'Field Pembeli Wajib Diisi!',
            // 'jumlah.required' => 'Field Tanggal Wajib Diisi!',
            // 'subtotal.required' => 'Field Harga Total Wajib Diisi!'
        ];

        $this->validate($request, $rule, $customMessages);
        
        $transaksi = new Transaksi;
        $transaksi->id_kasir = $request->id_kasir;
        $transaksi->id_pembeli = $request->id_pembeli;
        $transaksi->tanggal = $request->tanggal;
        $transaksi->harga_total = $request->harga_total;
        $status = $transaksi->save();


        foreach ($request->id_detail_produk as $key => $value) {
            $detailtransaksi = new DetailTransaksi;
            $detailtransaksi->id_transaksi = $transaksi->id;
            $detailtransaksi->id_detail_produk = $request->id_detail_produk[$key];
            $detailtransaksi->jumlah = $request->jumlah[$key];
            $detailtransaksi->subtotal = $request->subtotal[$key];
            $status = $detailtransaksi->save();
        }
        
        if ($status) {
            return redirect('transaksi')->with('success','Data berhasil diubah');
        }
        // else{
        //     return redirect('transaksi/create')->with('error', 'Data gagal ditambahkan');
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $transaksi = Transaksi::find($id);
        $data['transaksi'] = $transaksi;
        $data['kasir'] =User::where('role', 1)->get();
        $data['pembeli'] =User::where('role', 2)->get();
        return view('admin.transaksi.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // 
        $rule = [
            'kasir' => 'required',
            'pembeli' => 'required',
            'tanggal' => 'required',
            'harga_total' => 'required'   
        ];

        $customMessages = [
            'kasir.required' => 'Field Kasir Wajib Diisi!',
            'pembeli.required' => 'Field Pembeli Wajib Diisi!',
            'tanggal.required' => 'Field Tanggal Wajib Diisi!',
            'harga_total.required' => 'Field Harga Total Wajib Diisi!',
        ];

        $this->validate($request, $rule, $customMessages);

        $input = $request->all();

        $transaksi= Transaksi::find($id);
        $transaksi->kasir = $request->kasir;
        $transaksi->pembeli = $request->pembeli;
        $transaksi->tanggal = $request->tanggal;
        $transaksi->harga_total = $request->harga_total;

        $status = $transaksi->save();
        if ($status) {
            return redirect('transaksi')->with('success','Data berhasil diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user= Transaksi::find($id);
        $status = $user->delete();
        
        if ($status) {
            return 1;
        }else{
            return 0;
        
        }
    }


    public function search(Request $request)
    {
        $search = Transaksi::where('title','like',"%".$request->search."%")->paginate(2);
        return view('admin.transaksi.index',['transaksi'=>$search]);
    }

    public function search_product()
    {
        $query = request('query');
        $produk = DetailProduk::where("id_produk","like","%$query%")->get();
        
        return view('admin.transaksi.search', ['detail' => $produk]);
    }

    // public function cart()
    // {
    //     $cart = session('cart');
    //     return view('cart')->with('cart',$cart);
    // }

    public function tambah_cart($id)
    {
        $cart = session()->get('cart');

        // dd(request()->query());

        if(isset($cart[$id])){
            $cart[$id]['jumlah']++;
            session(['cart' => $cart]);
            return redirect('transaksi');
        };

        $detail = DetailProduk::where('id', 'like', request()->query('id_detail_product'))->first();
        $produk = Produk::find($id);
        $ukuran = Ukuran::find($detail->id_ukuran);
        // dd($detail);
        $cart[$id] = [
            "id" => $id,
            "id_detail_produk" => $detail->id,
            "id_produk" => $detail->id_produk,
            "nama_produk" => $produk->nama_produk,
            "ukuran" => $ukuran->ukuran,
            "harga" => $detail->harga,
            "jumlah" => 1
        ];

        session(['cart' => $cart]);
        // session()->put('cart', $cart);
        return redirect('transaksi');
    }

    public function hapus_cart($id)
    {
        $cart = session()->get('cart');
        unset($cart[$id]);
        session(['cart' => $cart]);
        return redirect('transaksi');
    }

    public function edit_quantity(Request $request, $id)
    {
        $cart = session()->get('cart');
        $cart[$id]['jumlah'] = $request->jumlah;
        session(['cart' => $cart]);
        return redirect('transaksi');
    }

    public function ltransaksi()
    {
        $data['logtransaksi'] = Transaksi::all();
        return view('admin.logtransaksi.index', $data);
    }




}



    
 
