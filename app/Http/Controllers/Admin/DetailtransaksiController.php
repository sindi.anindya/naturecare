<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DetailTransaksi;
use Illuminate\Http\Request;
use App\Models\Ukuran;
use App\Models\Produk;

class DetailtransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rule = [
            'id_transaksi' => 'required',
            'id_detail_produk' => 'required',
            'jumlah' => 'required',
            'subtotal' => 'required'   
        ];

        $customMessages = [
            'id_transaksi.required' => 'Field Kasir Wajib Diisi!',
            'id_detail_produk.required' => 'Field Pembeli Wajib Diisi!',
            'jumlah.required' => 'Field Tanggal Wajib Diisi!',
            'subtotal.required' => 'Field Harga Total Wajib Diisi!',
        ];

        $this->validate($request, $rule, $customMessages);
        
        $input = $request->all();
        $status = DetailTransaksi::create($input);
        if ($status) {
            return redirect('detailtransaksi')->with('success', 'Data berhasil ditambahkan');
        }else{
            return redirect('detailtransaksi/create')->with('error', 'Data gagal ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // 
        $rule = [
            'id_transaksi' => 'required',
            'id_detail_produk' => 'required',
            'jumlah' => 'required',
            'subtotal' => 'required'   
        ];

        $customMessages = [
            'id_transaksi.required' => 'Field Kasir Wajib Diisi!',
            'id_detail_produk.required' => 'Field Pembeli Wajib Diisi!',
            'jumlah.required' => 'Field Tanggal Wajib Diisi!',
            'subtotal.required' => 'Field Harga Total Wajib Diisi!',
        ];

        $this->validate($request, $rule, $customMessages);

        $input = $request->all();

        $detailtransaksi= DetailTransaksi::find($id);
        $detailtransaksi->id_transaksi = $request->id_transaksi;
        $detailtransaksi->id_detail_produk = $request->id_detail_produk;
        $detailtransaksi->jumlah = $request->jumlah;
        $detailtransaksi->subtotal = $request->subtotal;

        $status = $detailtransaksi->save();
        if ($status) {
            return redirect('detailtransaksi')->with('success','Data berhasil diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
    }

    
}

