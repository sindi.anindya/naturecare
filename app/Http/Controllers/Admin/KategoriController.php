<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Models\Kategori;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['kategori'] = Kategori::all();
        return view('admin.kategori.index', $data);
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.kategori.formkategori');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rule = [
            'kategori' => 'required',
            'keterangan' => 'required'
        ];

        $customMessages = [
            'kategori.required' => 'Field Kategori Wajib Diisi!',
            'keterangan.required' => 'Field Keterangan Wajib Diisi!',
        ];

        $this->validate($request, $rule, $customMessages);
        
        $input = $request->all();
        $status = Kategori::create($input);
        if ($status) {
            return redirect('kategori')->with('success','Data berhasil ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $kategori = Kategori::find($id);
        $data['kategori'] = $kategori;
        return view('admin.kategori.formkategori', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rule = [
            'kategori => required',
            'keterangan => required'
        ];
        $customMessages = [
            'kategori.required' => 'Field Kategori Wajib Diisi!',
            'keterangan.required' => 'Field Keterangan Wajib Diisi!',
        ];

        $this->validate($request, $rule, $customMessages);
        
        $kategori = Kategori::find($id);
        $kategori->kategori = $request->kategori;
        $kategori->keterangan = $request->keterangan;

        $status = $kategori->save();
        if ($status) {
            return redirect('kategori')->with('success','Data berhasil diubah');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $kategori = Kategori::find($id);
        $status = $kategori->delete();
        
        if ($status) {
            return 1;
        }else{
            return 0;
        
        }
    }
}
