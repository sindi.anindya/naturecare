<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DetailProduk;
use Illuminate\Http\Request;
use App\Models\Ukuran;
use App\Models\Produk;

class DetailprodukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['detailproduk'] = DetailProduk::all();
        return view('admin.detailproduk.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $listproduk ['listproduk'] = Produk::all();
        $data['ukuran'] = Ukuran::all();
        return view('admin.detailproduk.form', $listproduk, $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rule = [
            'id_produk => required',
            'id_ukuran => required',
            'stok => required',
            'hpp => required',
            'harga => required'

        ];
        $customMessages = [
            'id_produk.required' => 'Field Id Produk Wajib Diisi!',
            'id_ukuran.required' => 'Field Id Ukuran Wajib Diisi',
            'stok' => 'Field Stok Wajib Diisi',
            'hpp.required' => 'Field HPP Wajib Diisi!',
            'harga.required' => 'Field Harga Wajib Diisi!',
        ];

        $this->validate($request, $rule, $customMessages);
        
        $input = $request->all();

        $status = DetailProduk::create($input);
        if ($status){
            return redirect('/listproduk/'.$request->id_produk.'/edit')->with('success', 'Data berhasil diubah');
        }else{
            return redirect('/listproduk/'.$request->id_produk.'/edit')->with('error', 'Data gagal diubah');
        }
    

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $detailproduk = DetailProduk::find($id);
        $data['listproduk'] = Produk::all();
        $data ['ukuran'] = Ukuran::all();
        $data['detailproduk'] = $detailproduk;
        return view('admin.detailproduk.form', $data);
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rule = [
            'id_produk => required',
            'id_ukuran => required',
            'stok => required',
            'hpp => required',
            'harga => required'

        ];
        $customMessages = [
            'id_produk.required' => 'Field Id Produk Wajib Diisi!',
            'id_ukuran.required' => 'Field Id Ukuran Wajib Diisi',
            'stok' => 'Field Stok Wajib Diisi',
            'hpp.required' => 'Field HPP Wajib Diisi!',
            'harga.required' => 'Field Harga Wajib Diisi!',
        ];

        $this->validate($request, $rule, $customMessages);
        $input = $request->all();

        $detailproduk = DetailProduk::find($id);
        $detailproduk->id_produk= $request->id_produk;
        $detailproduk->id_ukuran = $request->id_ukuran;
        $detailproduk->stok = $request->stok;
        $detailproduk->hpp = $request->hpp;
        $detailproduk->harga = $request->harga;


        $status = $detailproduk->save();
        if ($status){
            return redirect('/listproduk/'.$request->id_produk.'/edit')->with('success', 'Data berhasil diubah');
        }else{
            return redirect('/listproduk/'.$request->id_produk.'/edit')->with('error', 'Data gagal diubah');
        }
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $detailproduk = DetailProduk::find($id);
        $status = $detailproduk->delete();
        
        if ($status) {
            return 1;
        }else{
            return 0;
        
        }
    }

    public function createWithProduk($id){
        $data['id_produk'] = $id;
        $data['ukuran'] = Ukuran::all();
        return view ('admin.detailproduk.form', $data);
    }
    public function storeWithProduk(Request $request, $id){
        $rule = [
            'id_produk' => 'required',
            'id_ukuran' => 'required',
            'stok' => 'required',
            'hpp' => 'required',
            'harga' => 'required'
        ];
        $customMessages = [
            'id_produk.required' => 'Field Id Produk Wajib Diisi!',
                'id_ukuran.required' => 'Field Id Ukuran Wajib Diisi',
                'stok' => 'Field Stok Wajib Diisi',
                'hpp.required' => 'Field HPP Wajib Diisi!',
                'harga.required' => 'Field Harga Wajib Diisi!',
            ];
        $this->validate($request, $rule, $customMessages);
        // $id_Produk = '11';
        // $request['id_produk'] = $id_Produk;
        $input = $request->all();
       
        $status = DetailProduk::create($input);
        if ($status){
            return redirect('detailproduk')->with('success', 'Data berhasil ditambahkan');
        }else{
            return redirect('detailproduk/create')->with('error', 'Data gagal Ditambahkan');
        }
    }
    
        
    
}

