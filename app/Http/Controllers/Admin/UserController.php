<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['user'] = User::all();
        return view('admin.user.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.user.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rule = [
            'nama' => 'required',
            'email' => 'required',
            'password',
            'no_telepon' => 'required',
            'role' => 'required',
            'alamat' => 'required'
        ];

        $customMessages = [
            'nama.required' => 'Field Nama Wajib Diisi!',
            'email.required' => 'Field Email Wajib Diisi!',
            'password.required' => 'Field Password Wajib Diisi!',
            'no_telepon.required' => 'Field Nomor Telepon Wajib Diisi!',
            'role.required' => 'Field Role Wajib Diisi!',
            'alamat.required' => 'Field Alamat Wajib Diisi!',
        ];

        $this->validate($request, $rule, $customMessages);
        
        $input = $request->all();
        $input["password"] = Hash::make($request->password);
        $status = User::create($input);
        if ($status) {
            return redirect('user')->with('success','Data berhasil ditambahkan');
        }
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::find($id);
        $data['user'] = $user;
        return view('admin.user.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rule = [
            'nama' => 'required',
            'email' => 'required',
            'password',
            'no_telepon' => 'required',
            'role' => 'required',
            'alamat' => 'required'
        ];

        $customMessages = [
            'nama.required' => 'Field Nama Wajib Diisi!',
            'email.required' => 'Field Email Wajib Diisi!',
            'password.required' => 'Field Password Wajib Diisi!',
            'no_telepon.required' => 'Field Nomor Telepon Wajib Diisi!',
            'role.required' => 'Field Role Wajib Diisi!',
            'alamat.required' => 'Field Alamat Wajib Diisi!',
        ];

        $this->validate($request, $rule, $customMessages);

        $user= User::find($id);
        $user->nama = $request->nama;
        $user->email = $request->email;
        // $user->password = $request->password;
        if($request->password)
        {
        $user->password = Hash::make($request->password);
        }
        $user->no_telepon = $request->no_telepon;
        $user->role = $request->role;
        $user->alamat = $request->alamat;

        $status = $user->save();
        if ($status) {
            return redirect('user')->with('success','Data berhasil diubah');
        }
        
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user= User::find($id);
        $status = $user->delete();
        
        if ($status) {
            return 1;
        }else{
            return 0;
        
        }
    }
}
    
