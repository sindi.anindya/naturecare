<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Ukuran;
use Illuminate\Http\Request;

class UkuranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['ukuran'] = Ukuran::all();
        return view('admin.kategori.ukuran', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.kategori.formukuran');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rule = [
            'ukuran' => 'required',
            'keterangan' => 'required'
        ];

        $customMessages = [
            'ukuran.required' => 'Field Ukuran Wajib Diisi!',
            'keterangan.required' => 'Field Keterangan Wajib Diisi!',
        ];

        $this->validate($request, $rule, $customMessages);
        
        $input = $request->all();
        $status = Ukuran::create($input);
        if ($status) {
            return redirect('ukuran')->with('success','Data berhasil ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $ukuran = Ukuran::find($id);
        $data['ukuran'] = $ukuran;
        return view('admin.kategori.formukuran', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rule = [
            'ukuran => required',
            'keterangan => required'
        ];
        $customMessages = [
            'ukuran.required' => 'Field Ukuran Wajib Diisi!',
            'keterangan.required' => 'Field Keterangan Wajib Diisi!',
        ];

        $this->validate($request, $rule, $customMessages);
        
        $ukuran= Ukuran::find($id);
        $ukuran->ukuran = $request->ukuran;
        $ukuran->keterangan = $request->keterangan;

        $status = $ukuran->save();
        if ($status) {
            return redirect('ukuran')->with('success','Data berhasil diubah');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $ukuran= Ukuran::find($id);
        $status = $ukuran->delete();
        
        if ($status) {
            return 1;
        }else{
            return 0;
        
        }
    }
}