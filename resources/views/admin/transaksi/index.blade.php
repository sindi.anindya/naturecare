@extends('admin.layout.app')

@section('content')


    <!-- BEGIN: Content-->
    <div class="app-content content ecommerce-application">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Transaksi</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#">eCommerce</a>
                                    </li>
                                    <li class="breadcrumb-item active">Shop
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                    <div class="form-group breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="app-todo.html"><i class="mr-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="mr-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="mr-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="mr-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-detached content-left">
                <div class="content-body">
                    <!-- E-commerce Content Section Starts -->
                    <section id="ecommerce-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="ecommerce-header-items">
                                    <div class="result-toggler">
                                        <button class="navbar-toggler shop-sidebar-toggler" type="button" data-toggle="collapse">
                                            <span class="navbar-toggler-icon d-block d-lg-none"><i data-feather="menu"></i></span>
                                        </button>
                                        <div class="search-results">16285 results found</div>
                                    </div>
                                    <div class="view-options d-flex">
                                        <div class="btn-group dropdown-sort">
                                            <button type="button" class="btn btn-outline-primary dropdown-toggle mr-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="active-sorting">Featured</span>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="javascript:void(0);">Featured</a>
                                                <a class="dropdown-item" href="javascript:void(0);">Lowest</a>
                                                <a class="dropdown-item" href="javascript:void(0);">Highest</a>
                                            </div>
                                        </div>
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-icon btn-outline-primary view-btn grid-view-btn">
                                                <input type="radio" name="radio_options" id="radio_option1" checked />
                                                <i data-feather="grid" class="font-medium-3"></i>
                                            </label>
                                            <label class="btn btn-icon btn-outline-primary view-btn list-view-btn">
                                                <input type="radio" name="radio_options" id="radio_option2" />
                                                <i data-feather="list" class="font-medium-3"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- E-commerce Content Section Starts -->

                    <!-- background Overlay when sidebar is shown  starts-->
                    <div class="body-content-overlay"></div>
                    <!-- background Overlay when sidebar is shown  ends-->
                    

                    <!-- E-commerce Search Bar Starts -->
                        <div class="row mt-1">
                            <div class="col-sm-12">
                            <form action="searchproduct" method="get" class="input-group">
                                <div class="input-group">
                                <input type="search" class="form-control search-product" placeholder="Cari Produk" name="query" />
                                <div class="input-group-append">
                                    <button type="submit" class="input-group-text"><i data-feather="search"></i></button>
                                <!-- <span class="input-group-text"><i data-feather="search" class="text-muted"></i></span> -->
                                </div>
                            </div>
                            </form>
                            </div>
                        </div>
                    <!-- E-commerce Search Bar Ends -->

                    <!-- E-commerce Products Starts -->
                    <section id="ecommerce-products" class="grid-view">
                        @foreach($produk as $product)
                        <div class="card ecommerce-card">
                            <div class="item-img text-center">
                                <a href="app-ecommerce-details.html">
                                    <img class="img-fluid card-img-top" src="{{url('') . '/' . $product->foto}}" alt="img-placeholder" style="height: 350px; object-fit: cover; object-position: center;"/></a>
                            </div>
                            <div class="card-body">
                                <h6 class="item-name">
                                    <a class="text-body" href="app-ecommerce-details.html">{{$product->nama_produk}}</a>
                                </h6>
                                {{-- <a href="">{{ $product->produk_detail->ukuran->ukuran}}</a> --}}
                                <p class="card-text item-description">
                                    {{$product->keterangan}}
                                </p>
                                <h6 class="item-rating">
                                    <select class="form-control select-ukuran" id="id_ukuran_{{$product->id}}" id_product="{{$product->id}}" name="id_ukuran"  required>
                                        <option value="">Select Ukuran</option>
                                        @foreach ($product->detail_produk as $value)
                                        <option value="{{$value->id}}">
                                            {{$value->ukuran->ukuran}}
                                             - {{$value->harga}}
                                            </option>
                                        @endforeach
                                    </select>
                                </h6>
                                <h6 class="item-price">
                                    {{$product->detail_produk[0]->harga}} - {{$product->detail_produk[count($product->detail_produk) - 1]->harga}}
                                    {{-- @foreach ($detail as $value)
                                        {{$value->harga}}
                                    @endforeach --}}
                                </h6>
                            </div>
                           
                            <div class="item-options text-center">
                                <div class="item-wrapper">
                                    <div class="item-cost">
                                        <h4 class="item-price"></h4>
                                    </div>
                                </div>
                                <form action="cart/tambah/{{$product->id}}">
                                    <input type="text" hidden name="id_detail_product" id="id_detail_product_{{$product->id}}" value="">
                                    <button class="btn btn-primary btn-cart" style="padding-right : 57px;">
                                        {{-- class="btn btn-primary btn-block btn-next place-order" --}}
                                        <i class= "ml-2" data-feather="shopping-cart"></i>
                                        <span class="add-to-cart">Tambahkan Ke Keranjang</span>
                                    </button>
                                </form>
                                
                            </div>
                        </div>
                        @endforeach
                    </section>
                    <!-- E-commerce Products Ends -->

                </div>
            </div>
            @if(empty($cart) || count($cart) == 0)
            @else
            <form class="form" method="POST" action="{{route('transaksi.store')}}" enctype="multipart/form-data">
                @csrf
            <div class="sidebar-detached sidebar-right">
                <div class="sidebar">
                    <!-- Ecommerce Sidebar Starts -->
                    <div class="sidebar-shop">
                                    <div class="card">
                                        <div class="card-body">
                                            {{-- <label class="section-label mb-1">Produk</label> --}}
                                            <div class="col-14">
                                                <div class="form-group row">
                                                    <label for="email-id">Kasir</label>
                                                    <div class="col-sm-12">
                                                        <div class="input-group input-group-merge">
                                                            <select class="form-control select" id="id_kasir" name="id_kasir" required>
                                                                <option value="">-- PILIH KASIR --</option>
                                                                @foreach ($kasir as $row)
                                                                <option value="{{$row->id}}">
                                                                {{$row->nama}}
                                                                </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <a href="{{route('user.create')}}" class="btn btn-primary">Tambah Kasir</a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-14">
                                                    <div class="form-group row">
                                                    <label for="email-id">Pembeli</label>
                                                        <div class="col-sm-12">
                                                            <div class="input-group input-group-merge">
                                                                <select class="form-control select" id="id_pembeli" name="id_pembeli" required>
                                                                    <option value="">-- PILIH PEMBELI --</option>
                                                                    @foreach ($pembeli as $row)
                                                                    <option value="{{$row->id}}">
                                                                    {{$row->nama}}
                                                                    </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <a href="{{route('user.create')}}" class="btn btn-primary">Tambah Pembeli</a>
                                                            </div>
                                                        </div>
                                                    </div>

                                            <center><h4>Keranjang</h4></center>
                                            <hr />
                                            <h5>Detail Produk</h5><br>
                                            
                                            @foreach($cart as $ct => $crt)
                                            @php
                                                $itotal = $crt['harga'] * $crt['jumlah'];
                                            @endphp
                                            <div class="item-quantity mb-2">
                                                <div class="input-group quantity-counter-wrapper">
                                                    <div class="row">
                                                        <div class="col">
                                                            <span class="quantity-title">{{$crt['nama_produk']}}  -  {{$crt['ukuran']}}</span>
                                                            <input type="hidden" name="id_detail_produk[]" id="id_detail_produk" value="{{$crt['id_detail_produk']}}">
                                                        </div>
                                                        <input type="hidden" class="iprice" value="{{$crt['harga']}}">
                                                        <div class="col">
                                                            <div class="item-quantity">
                                                                <div class="input-group quantity-counter-wrapper">
                                                                    {{-- <span class="input-group-btn input-group-prepend bootstrap-touchspin-injected"><button class="btn btn-primary bootstrap-touchspin-down" type="button">-</button></span> --}}
                                                                    <input type="text" name="qty[]" id="qty" class="quantity-counter form-control" value="{{$crt['jumlah']}}">
                                                                    <input type="hidden" name="jumlah[]" id="jumlah" value="{{$crt['jumlah']}}">
                                                                    {{-- <span class="input-group-btn input-group-append bootstrap-touchspin-injected"><button class="btn btn-primary bootstrap-touchspin-up" type="button">+</button></span> --}}
                                                                </div>
                                                            </div>

                                                            <center class="mt-1">
                                                            <p class="itotal" id="price1">{{$itotal}}</p>
                                                            <input type="hidden" name="subtotal[]" id="subtotal" value="{{$itotal}}">
                                                            
                                                            <a href="cart/hapus/{{$ct}}" class="btn btn-primary btn-sm"><i data-feather="trash"></i></a>
                                                            </center>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach 
                                            <hr>
                                            <center><h4>Harga</h4></center>
                                            <hr />
                                            <div class="price-details">
                                                <h6 class="price-title">Detail Harga</h6><br>
                                                <ul class="list-unstyled">
                                                @php
                                                    $jumlahtotal = 0;
                                                @endphp
                                                @foreach($cart as $ct => $crt)
                                                    @php
                                                        $subtotal = $crt['harga'] * $crt['jumlah'];
                                                    @endphp
                                                    <li class="price-detail">
                                                        <div class="row">
                                                            <div class="col">{{$crt['nama_produk']}}</div>
                                                            <div class="col text-right">{{$subtotal}}</div>
                                                        </div>
                                                    </li><br>
                                                    @php
                                                        $jumlahtotal+= $subtotal;
                                                    @endphp
                                                @endforeach
                                                </ul>
                                                <hr />
                                                <ul class="list-unstyled">
                                                    <li class="price-detail">
                                                        <div class="row">
                                                            <div class="col">Total</div>
                                                            <div class="col font-weight-bolder text-right">RP {{$jumlahtotal}}</div>
                                                            <input type="hidden" name="harga_total" class="form-control" value="{{$jumlahtotal}}">
                                                        </div>
                                                    </li>
                                                </ul>
                                                <hr>
                                                    <div class="form-group">
                                                        <label for="password-icon">Uang Pembeli</label>
                                                        <div class="input-group input-group-merge">
                                                        <input type="text" id="harga" class="form-control" name="harga" placeholder="Harga"/>
                                                        </div>
                                                    </div>
                                                <ul class="list-unstyled">
                                                    <li class="price-detail">
                                                        <div class="detail-title detail-total">Kembalian</div>
                                                        <div class="detail-amt font-weight-bolder">$7</div>
                                                    </li>
                                                </ul>
                                                <hr>

                                                {{-- <input type="hidden" name="id_transaksi" id="id_transaksi" value="{{$transaksi}}"> --}}
                                                {{-- <td>
                                                    <a href="{{route('detailtransaksi.index')}}" class="btn btn-primary">Simpan </a>
                                                    <a href="#" data-id="{{$row->id}}" class="btn btn-danger btn-primary">Cetak</a>
                                                </td> --}}
                                                <button class="btn btn-primary btn-save" id="confirm-color">Simpan</button>
                                                <button type="submit" class="btn btn-primary">Cetak</button><br>
                                                {{-- <button type="submit" class="btn btn-primary btn-center">Batal</button> --}}
                                            </div>
                                        </div>
                                    </div>
                    </div>
                    <!-- Ecommerce Sidebar Ends -->

                </div>
            </div>
        </form>
            @endif
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset ('assets/admin/') }}/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset ('assets/admin/') }}/vendors/js/extensions/wNumb.min.js"></script>
    <script src="{{ asset ('assets/admin/') }}/vendors/js/extensions/nouislider.min.js"></script>
    <script src="{{ asset ('assets/admin/') }}/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js"></script>
    <script src="{{ asset ('assets/admin/') }}/vendors/js/extensions/toastr.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset ('assets/admin/') }}/js/core/app-menu.js"></script>
    <script src="{{ asset ('assets/admin/') }}/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset ('assets/admin/') }}/js/scripts/pages/app-ecommerce.js"></script>
    <script src="{{ asset ('assets/admin/') }}/js/scripts/pages/app-ecommerce-checkout.js"></script>
    <!-- END: Page JS-->

    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
        $('.select-ukuran').on('change', function () {
            $id_product = $(this).attr('id_product');
            console.log('#id_detail_product_'+$id_product);
            // console.log("asdasdk")
            $('#id_detail_product_'+$id_product).val(this.value);
            // console.log($('input[id=id_detail_product_'+$id_product+']').val());
        });
        $(".bootstrap-touchspin-up").on('click', function() {
            //FUNCTION ++
            var value = parseInt(document.getElementById('qty').value,10);
            if(value<10){
               value++;
               document.getElementById('qty').value = value;
            }
            
        });
        $(".bootstrap-touchspin-down").on('change', function() {
            //FUNCTION --

        });
        $(document).on('click', '.btn-save', function () {
        // var id = $(this).data('.confirm-color');
        Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to save this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#6cd950',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, saved it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        'url': '{{url('transaksi')}}',
                        'type': 'post',
                        'data': {
                            '_method': 'POST',
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (response) {
                            if (response == 1) {
                                Swal.fire(
                                    'Data Berhasil Disimpan!',
                                    'Your file has been saved.',
                                    'success'
                                )
                                window.setTimeout(function () {
                                    location.reload();
                                }, 2000);
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Data gagal disimpan!'
                                });
                            }
                        }
                    });
                }
            });
        });
        var iprice=document.getElementsByClassName('iprice');
        var iquantity=document.getElementsByClassName('iquantity');
        var itotal=document.getElementsByClassName('itotal');

        // $('#id_detail_product_40').val('12');
        console.log("asd");

        
        // //plus button 
        // document.querySelector(".bootstrap-touchspin-up").addEventListener("click", function()
        // {
        //     //getting value of input 
        //     valueCount = document.getElementById("quantity").value;

        //     //input value increment by 1
        //     valueCount++;

        //     //setting increment input value
        //     document.getElementById("quantity").value = valueCount

        //     if(valueCount >  1)
        //     {
        //         document.querySelector(".bootstrap-touchspin-down").removeAttribute("disabled")
        //         document.querySelector(".bootstrap-touchspin-down").classList.remove("disabled")
        //     }

        //     //calling price function
        //     priceTotal()
        // })

        // //plus button 
        // document.querySelector(".bootstrap-touchspin-down").addEventListener("click", function()
        // {
        //     //getting value of input
        //     valueCount = document.getElementById("quantity").value;

        //     //input value increment by 1
        //     valueCount--;

        //     //setting increment input value
        //     document.getElementById("quantity").value = valueCount

        //     if(valueCount == 1)
        //     {
        //         document.querySelector(".bootstrap-touchspin-down").setAttribute("disabled", "disabled")
        //     }

        //     //calling price function
        //     priceTotal()
        // })
    </script>


@endsection

