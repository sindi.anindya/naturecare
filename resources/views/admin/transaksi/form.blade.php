@extends('admin.layout.app')

@section('content')
    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Form Layouts</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('/admin/transaksi/index')}}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#">Forms</a>
                                    </li>
                                    <li class="breadcrumb-item active"><a href="#">Form Layouts</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                    <div class="form-group breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="app-todo.html"><i class="mr-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="mr-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="mr-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="mr-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic Horizontal form layout section start -->
                <section id="basic-horizontal-layouts">
                    <div class="row justify-content-center">
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Transaksi</h4>
                                </div>
                                <div class="card-body">
                                    <form class="form form-horizontal" method="POST" action="{{@$transaksi ? route('transaksi.update',$transaksi->id) : route('transaksi.store')}}" enctype="multipart/form-data">
                                        @csrf
                                        @if (@$transaksi)
                                        @method('patch')
                                    @endif
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-sm-3 col-form-label">
                                                    <label for="email-id">Kasir</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="input-group input-group-merge">
                                                        <select class="form-control select" id="id_kasir" name="id_kasir" required>
                                                            <option value="">-- PILIH KASIR --</option>
                                                            @foreach ($kasir as $row)
                                                            <option value="{{$row->id}}" {{@$transaksi->id_kasir == $row->id ? "selected" : "" }}>
                                                            {{$row->nama}}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="email-id">Pembeli</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="input-group input-group-merge">
                                                            <select class="form-control select" id="id_pembeli" name="id_pembeli" required>
                                                                <option value="">-- PILIH PEMBELI --</option>
                                                                @foreach ($pembeli as $row)
                                                                <option value="{{$row->id}}" {{@$transaksi->id_pembeli == $row->id ? "selected" : "" }}>
                                                                {{$row->nama}}
                                                                </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-sm-3 col-form-label">
                                                    <label for="email-id">Tanggal</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <input type="date" id="tanggal" class="form-control" name="tanggal" placeholder="Tanggal" value="{{old('tanggal', @$transaksi? $transaksi->tanggal: '')}}"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-sm-3 col-form-label">
                                                    <label for="email-id">Harga Total</label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <input type="text" id="harga_total" class="form-control" name="harga_total" placeholder="Harga Total" value="{{old('harga_total', @$transaksi ? $transaksi->harga_total: '')}}"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                    <div class="col-sm-9 offset-sm-3">
                                        <button type="submit" class="btn btn-primary mr-1">Submit</button>
                                        <a href="{{route('transaksi.index')}}" class="btn btn-outline-secondary">Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                </section>
                <!-- Basic Horizontal form layout section end -->
            </div>
        </div>
    </div>
    <!-- END: Content-->
@endsection