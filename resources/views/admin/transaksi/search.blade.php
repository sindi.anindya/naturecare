@extends('admin.layout.app')

@section('content')


    <!-- BEGIN: Content-->
    <div class="app-content content ecommerce-application">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Shop</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#">eCommerce</a>
                                    </li>
                                    <li class="breadcrumb-item active">Shop
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                    <div class="form-group breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="app-todo.html"><i class="mr-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="mr-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="mr-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="mr-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-detached content-left">
                <div class="content-body">
                    <!-- E-commerce Content Section Starts -->
                    <section id="ecommerce-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="ecommerce-header-items">
                                    <div class="result-toggler">
                                        <button class="navbar-toggler shop-sidebar-toggler" type="button" data-toggle="collapse">
                                            <span class="navbar-toggler-icon d-block d-lg-none"><i data-feather="menu"></i></span>
                                        </button>
                                        <div class="search-results">16285 results found</div>
                                    </div>
                                    <div class="view-options d-flex">
                                        <div class="btn-group dropdown-sort">
                                            <button type="button" class="btn btn-outline-primary dropdown-toggle mr-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="active-sorting">Featured</span>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="javascript:void(0);">Featured</a>
                                                <a class="dropdown-item" href="javascript:void(0);">Lowest</a>
                                                <a class="dropdown-item" href="javascript:void(0);">Highest</a>
                                            </div>
                                        </div>
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-icon btn-outline-primary view-btn grid-view-btn">
                                                <input type="radio" name="radio_options" id="radio_option1" checked />
                                                <i data-feather="grid" class="font-medium-3"></i>
                                            </label>
                                            <label class="btn btn-icon btn-outline-primary view-btn list-view-btn">
                                                <input type="radio" name="radio_options" id="radio_option2" />
                                                <i data-feather="list" class="font-medium-3"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- E-commerce Content Section Starts -->

                    <!-- background Overlay when sidebar is shown  starts-->
                    <div class="body-content-overlay"></div>
                    <!-- background Overlay when sidebar is shown  ends-->

                    <!-- E-commerce Search Bar Starts -->
                        <div class="row mt-1">
                            <div class="col-sm-12">
                            <form action="searchproduct" method="get" class="input-group">
                                <div class="input-group">
                                <input type="search" class="form-control search-product" placeholder="Cari Produk" name="query" />
                                <div class="input-group-append">
                                    <button type="submit" class="input-group-text"><i data-feather="search"></i></button>
                                <!-- <span class="input-group-text"><i data-feather="search" class="text-muted"></i></span> -->
                                </div>
                            </div>
                            </form>
                            </div>
                        </div>
                    <!-- E-commerce Search Bar Ends -->

                    <!-- E-commerce Products Starts -->
                    <section id="ecommerce-products" class="grid-view">
                        @foreach($detail as $product)
                        <div class="card ecommerce-card">
                            <div class="item-img text-center">
                                <a href="app-ecommerce-details.html">
                                    <img class="img-fluid card-img-top" src="{{url('') . '/' . $product->produk['foto']}}" alt="img-placeholder" /></a>
                            </div>
                            <div class="card-body">
                                <h6 class="item-name">
                                    <a class="text-body" href="app-ecommerce-details.html">{{$product->produk['nama_produk']}}</a>
                                </h6>
                                <a href="">{{ $product->ukuran->ukuran}}</a>
                                <p class="card-text item-description">
                                    {{$product->produk['keterangan']}}
                                </p>
                                <div>
                                    <h6 class="item-price">RP {{$product->harga}}</h6>
                                </div>
                            </div>
                            <div class="item-options text-center">
                                <div class="item-wrapper">
                                    <div class="item-cost">
                                        <h4 class="item-price"></h4>
                                    </div>
                                </div>
                                <a href="javascript:void(0)" class="btn btn-primary btn-block">
                                    <i data-feather="shopping-cart"></i>
                                    <span class="add-to-cart">Tambah Ke Keranjang</span>
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </section>
                    <!-- E-commerce Products Ends -->

                </div>
            </div>
            <div class="sidebar-detached sidebar-right">
                <div class="sidebar">
                    <!-- Ecommerce Sidebar Starts -->
                    <div class="sidebar-shop">
                                    <div class="card">
                                        <div class="card-body">
                                            {{-- <label class="section-label mb-1">Produk</label> --}}
                                            <center><h4>Keranjang</h4></center>
                                            <hr />
                                            <h5>Detail Produk</h5><br>
                                            <div class="item-quantity mb-2">
                                                <div class="input-group quantity-counter-wrapper">
                                                    <div class="row">
                                                        <div class="col">
                                                            <span class="quantity-title">Garnier</span>
                                                        </div>
                                                        <div class="col">
                                                            <div class="input-group quantity-counter-wrapper">
                                                            <input type="text" class="quantity-counter" value="1" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-quantity">
                                                <div class="input-group quantity-counter-wrapper">
                                                    <div class="row">
                                                        <div class="col">
                                                            <span class="quantity-title">Pixy</span>
                                                        </div>
                                                        <div class="col">
                                                            <div class="input-group quantity-counter-wrapper">
                                                            <input type="text" class="quantity-counter" value="1" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <center><h4>Harga</h4></center>
                                            <hr />
                                            <div class="price-details">
                                                <h6 class="price-title">Detail Harga</h6><br>
                                                <ul class="list-unstyled">
                                                    <li class="price-detail">
                                                        <div class="detail-title">Total MRP</div>
                                                        <div class="detail-amt">$598</div>
                                                    </li>
                                                    <li class="price-detail">
                                                        <div class="detail-title">Bag Discount</div>
                                                        <div class="detail-amt discount-amt text-success">-25$</div>
                                                    </li>
                                                    <li class="price-detail">
                                                        <div class="detail-title">Estimated Tax</div>
                                                        <div class="detail-amt">$1.3</div>
                                                    </li>
                                                    <li class="price-detail">
                                                        <div class="detail-title">EMI Eligibility</div>
                                                        <a href="javascript:void(0)" class="detail-amt text-primary">Details</a>
                                                    </li>
                                                    <li class="price-detail">
                                                        <div class="detail-title">Delivery Charges</div>
                                                        <div class="detail-amt discount-amt text-success">Free</div>
                                                    </li>
                                                </ul>
                                                <hr />
                                                <ul class="list-unstyled">
                                                    <li class="price-detail">
                                                        <div class="detail-title detail-total">Total</div>
                                                        <div class="detail-amt font-weight-bolder">$574</div>
                                                    </li>
                                                </ul>
                                                <button type="submit" class="btn btn-primary simpan">Simpan</button>
                                            </div>
                                        </div>
                                    </div>
                    </div>
                    <!-- Ecommerce Sidebar Ends -->

                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>


    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset ('assets/admin/') }}/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset ('assets/admin/') }}/vendors/js/extensions/wNumb.min.js"></script>
    <script src="{{ asset ('assets/admin/') }}/vendors/js/extensions/nouislider.min.js"></script>
    <script src="{{ asset ('assets/admin/') }}/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js"></script>
    <script src="{{ asset ('assets/admin/') }}/vendors/js/extensions/toastr.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset ('assets/admin/') }}/js/core/app-menu.js"></script>
    <script src="{{ asset ('assets/admin/') }}/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset ('assets/admin/') }}/js/scripts/pages/app-ecommerce.js"></script>
    <script src="{{ asset ('assets/admin/') }}/js/scripts/pages/app-ecommerce-checkout.js"></script>
    <!-- END: Page JS-->

    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>

@endsection