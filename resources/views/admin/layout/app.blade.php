<!DOCTYPE html>
<html class="loading dark-layout" lang="en" data-layout="dark-layout" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Naturecare</title>
    <link rel="apple-touch-icon" href="{{ asset ('assets/admin') }}/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset ('assets/admin') }}/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset ('assets/admin') }}/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('assets/admin') }}/vendors/css/charts/apexcharts.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('assets/admin') }}/vendors/css/extensions/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('assets/admin') }}/vendors/css/extensions/sweetalert2.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/admin/')}}/vendors/css/extensions/nouislider.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset ('assets/admin') }}/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('assets/admin') }}/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('assets/admin') }}/css/colors.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('assets/admin') }}/css/components.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('assets/admin') }}/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('assets/admin') }}/css/themes/bordered-layout.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('assets/admin') }}/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset ('assets/admin') }}/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('assets/admin') }}/css/pages/dashboard-ecommerce.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('assets/admin') }}/css/plugins/charts/chart-apex.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('assets/admin') }}/css/plugins/extensions/ext-component-toastr.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('assets/admin/')}}/css/plugins/extensions/ext-component-sliders.css">
    <link rel="stylesheet" type="text/css" href="{{ asset ('assets/admin/')}}/css/pages/app-ecommerce.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="../../../assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="">

    <!-- BEGIN: Header-->
    @include('admin.layout.navbar')
    <!-- END: Header-->

    <!-- BEGIN: Main Menu-->
    @include('admin.layout.sidebar')
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    @yield('content')
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    @include('admin.layout.footer')
    <!-- END: Footer-->

    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    
    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset ('assets/admin/') }}/vendors/js/vendors.min.js"></script>
    <script src="{{ asset ('assets/admin/') }}/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset ('assets/admin/') }}/vendors/js/charts/apexcharts.min.js"></script>
    <script src="{{ asset ('assets/admin/') }}/vendors/js/extensions/toastr.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset ('assets/admin/') }}/js/core/app-menu.js"></script>
    <script src="{{ asset ('assets/admin/') }}/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset ('assets/admin/') }}/js/scripts/pages/dashboard-ecommerce.js"></script>
    <script src="{{asset('assets/admin/')}}/js/scripts/pages/app-ecommerce.js"></script>
    <!-- END: Page JS-->

    <!-- BEGIN: New JS -->
    <script src="script.js"></script>
    <!-- END: New JS -->

    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>

    <script>
        $(document).ready(function(){
            @if(Session::has('success'))
            var success = '{{Session::get("success")}}';
            toastr['success'](success);
            @endif
        });
    </script>

    @stack('scripts')
</body>
<!-- END: Body-->

</html>