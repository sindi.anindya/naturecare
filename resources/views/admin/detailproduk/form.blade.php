@extends('admin.layout.app')

@section('content')
    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Form Layouts</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('/admin/detailproduk/index')}}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#">Forms</a>
                                    </li>
                                    <li class="breadcrumb-item active"><a href="#">Form Layouts</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                    <div class="form-group breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="app-todo.html"><i class="mr-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="mr-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="mr-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="mr-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic Horizontal form layout section start -->
                <section id="basic-horizontal-layouts">
                    <div class="row justify-content-center">
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Detail Produk</h4>
                                </div>
                                <div class="card-body">
                                    @if ($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                    <h4 class="alert-heading">Error!</h4>
                                    @foreach ($errors->all() as $error)
                                        <div class="alert-body">
                                            {{$error}}
                                        </div>
                                    @endforeach
                                    </div>
                                    @endif
                                    <form class="form form-horizontal" method="POST" action="{{@$detailproduk ? route('detailproduk.update',@$detailproduk->id) : url('admin/detailproduk/{id_produk}'. @$detailProduk->id)}}" enctype="multipart/form-data">
                                        @csrf
                                        @if (@$detailproduk)
                                        @method('patch')
                                        @endif
                                        <div class="row">
                                        
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label"></div>
                                                    <div class="col-sm-9">
                                                        <input type="hidden" name="id_produk" class="form-control" value="{{@$detailproduk ? $detailproduk->id_produk : $id_produk }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="id_kategori">Ukuran</label>
                                                    </div>
                                                    <div class="col-9">
                                                        <select class="form-control select2" id="id_ukuran" name="id_ukuran">
                                                            <option value="">-- PILIH UKURAN --</option>
                                                            @foreach ($ukuran as $row)
                                                            <option value="{{$row->id}}" {{@$detailproduk->id_ukuran == $row->id ? "selected" : ''}}>
                                                            {{$row->ukuran}}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                        <div class="col-sm-9 offset-sm-3">
                                                            <a href="{{route ('ukuran.create')}}" class="btn btn-outline-secondary">Tambah Ukuran</a>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="email-id">Stok</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="stok" class="form-control" name="stok" placeholder="stok" value="{{old('stok', @$detailproduk ? $detailproduk->stok: '')}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="email-id">HPP</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="hpp" class="form-control" name="hpp" placeholder="hpp" value="{{old('hpp', @$detailproduk ? $detailproduk->hpp: '')}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="email-id">Harga</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="harga" class="form-control" name="harga" placeholder="harga" value="{{old('harga', @$detailproduk ? $detailproduk->harga: '')}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-9 offset-sm-3">
                                                <button type="submit" class="btn btn-primary mr-1">Submit</button>
                                                <button type="reset" class="btn btn-outline-secondary">Reset</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                </section>
                <!-- Basic Horizontal form layout section end -->
            </div>
        </div>
    </div>
    <!-- END: Content-->
@endsection