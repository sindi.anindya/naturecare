@extends('admin.layout.app')

@section('content')
    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Form Layouts</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('/admin/kategori/listproduk')}}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#">Forms</a>
                                    </li>
                                    <li class="breadcrumb-item active"><a href="#">Form Layouts</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                    <div class="form-group breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="app-todo.html"><i class="mr-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="mr-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="mr-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="mr-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic Horizontal form layout section start -->
                <section id="basic-horizontal-layouts">
                    <div class="row justify-content-center">
                        <div class="col-md-12 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{@$listproduk ? 'Ubah' : 'Tambah'}} Produk</h4>
                                </div>
                                <div class="card-body">
                                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                         <ul>
                                            @foreach ($errors->all() as $error)
                                                 <li>{{ $error }}</li>
                                            @endforeach
                                         </ul>
                                    </div>
                                    @endif
                                    <form class="form form-horizontal" method="POST" action="{{@$listproduk ? route('listproduk.update',@$listproduk->id) : route('listproduk.store')}}" enctype="multipart/form-data">
                                        
                                        @csrf
                                        @if (@$listproduk)
                                            @method('patch')

                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="id_kategori">Kategori</label>
                                                    </div>
                                                    <div class="col-9">
                                                        <select class="form-control select2" id="id_kategori" name="id_kategori">
                                                            <option value="">-- PILIH KATEGORI --</option>
                                                            @foreach ($kategori as $row)
                                                            <option value="{{$row->id}}" {{@$listproduk->id_kategori == $row->id ? 'selected' : ''}}>
                                                            {{$row->kategori}}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                        <div class="col-sm-9 offset-sm-3">
                                                            <a href="{{route ('kategori.create')}}" class="btn btn-outline-secondary">Tambah kategori</a>
                                                        </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="nama-produk">Nama Produk</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input type="text" id="nama_produk" class="form-control" name="nama_produk" placeholder="Nama Produk" value="{{old('nama_produk', @$listproduk ? $listproduk->nama_produk : '')}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="customFile">Foto</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input" id="foto" name="foto" />
                                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-sm-3 col-form-label">
                                                        <label for="email-id">Keterangan</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                       <textarea name="keterangan" id="keterangan" cols="30" rows="10" class="form-control">{{old('keterangan', @$listproduk ? $listproduk->keterangan : '')}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-9 offset-sm-3">
                                                <a href="{{route('kategori.index')}}" class="btn btn-outline-secondary">Back</a>
                                                <button type="submit" class="btn btn-primary mr-1">Submit</button>
                                            </div>
                                        </div>
                                    </form><br>

                                    <div class="divider divider-info">
                                        <div class="divider-text"><h4>Detail Produk</h4></div>
                                    </div>
                                        <a href="{{url('admin/detailproduk/'. $listproduk->id . '/tambah')}}" class="btn btn-primary">Tambah Data</a>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Produk</th>
                                                <th>Ukuran</th>
                                                <th>Stok</th>
                                                <th>HPP</th>
                                                <th>Harga</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($detailproduk as $row)
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$row->produk->nama_produk}}</td>
                                            <td>{{$row->ukuran->ukuran}}</td> 
                                            <td>{{$row->stok}}</td>
                                            <td>{{$row->hpp}}</td>
                                            <td>{{$row->harga}}</td>
    
                                            <td>
                                                <a href="{{route('detailproduk.edit', $row->id)}}" class="btn btn-warning">Edit </a>
                                                <a href="#" data-id="{{$row->id}}" class="btn btn-danger btn-del">Delete</a>
                                            </td>
                                        </tbody>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                </section>
                
@push('scripts')
<script>
    $(document).on('click', '.btn-del', function () {
        var id = $(this).data('id');
        Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        'url': '{{url('detailproduk')}}/' + id,
                        'type': 'post',
                        'data': {
                            '_method': 'DELETE',
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (response) {
                            if (response == 1) {
                                Swal.fire(
                                    'Deleted!',
                                    'Your file has been deleted.',
                                    'success'
                                )
                                window.setTimeout(function () {
                                    location.reload();
                                }, 2000);
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Data gagal dihapus!'
                                });
                            }
                        }
                    });
                }
            });
        });
    </script>
@endpush


                
    @else
        <div class="row">
            <div class="col-12">
                <div class="form-group row">
                     <div class="col-sm-3 col-form-label">
                         <label for="id_kategori">Kategori</label>
                     </div>
                <div class="col-9">
                    <select class="form-control select2" id="id_kategori" name="id_kategori">
                        <option value="">-- PILIH KATEGORI --</option>
                        @foreach ($kategori as $row)
                        <option value="{{$row->id}}" {{@$listproduk->id_kategori == $row->id ? 'selected' : ''}}>
                        {{$row->kategori}}
                        </option>
                        @endforeach
                    </select>
                </div>
                    <div class="col-sm-9 offset-sm-3">
                        <a href="{{route ('kategori.create')}}" class="btn btn-outline-secondary">Tambah kategori</a>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group row">
                    <div class="col-sm-3 col-form-label">
                        <label for="nama-produk">Nama Produk</label>
                    </div>
                <div class="col-sm-9">
                    <input type="text" id="nama_produk" class="form-control" name="nama_produk" placeholder="Nama Produk" value="{{old('nama_produk', @$listproduk ? $listproduk->nama_produk : '')}}"/>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group row">
                <div class="col-sm-3 col-form-label">
                    <label for="customFile">Foto</label>
                </div>
                <div class="col-sm-9">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="foto" name="foto" />
                        <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group row">
                <div class="col-sm-3 col-form-label">
                    <label for="email-id">Keterangan</label>
                </div>
                <div class="col-sm-9">
                   <textarea name="keterangan" id="keterangan" cols="30" rows="10" class="form-control">{{old('keterangan', @$listproduk ? $listproduk->keterangan : '')}}</textarea>
                </div>
            </div>
        </div>
        <div class="col-sm-9 offset-sm-3">
            <a href="{{route('kategori.index')}}" class="btn btn-outline-secondary">Back</a>
            <button type="submit" class="btn btn-primary mr-1">Submit</button>
        </div>
    </div>
</form>
    @endif
                <!-- Basic Horizontal form layout section end -->
            </div>
        </div>
    </div>
    <!-- END: Content-->
@endsection