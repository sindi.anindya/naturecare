<?php

use App\Http\Controllers\Admin\DashboardController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\KategoriController;
use App\Http\Controllers\Admin\ListprodukController;
use App\Http\Controllers\Admin\UkuranController;
use App\Http\Controllers\Admin\DetailprodukController;
use App\Http\Controllers\Admin\TransaksiController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\DetailtransaksiController;
use App\Http\Controllers\Admin\LogtransaksiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('listproduk', ListprodukController::class);
Route::resource('kategori', KategoriController::class);  
Route::resource('ukuran', UkuranController::class);
Route::resource('detailproduk', DetailprodukController::class);
Route::get('/admin/detailproduk/{id_produk}/tambah', [DetailProdukController::class, 'createWithProduk']);
Route::post('/admin/detailproduk/{id_produk}', [DetailProdukController::class, 'store']);
Route::resource('transaksi', TransaksiController::class);
Route::resource('user', UserController::class);
Route::resource('detailtransaksi', DetailtransaksiController::class);
Route::resource('logtransaksi', LogtransaksiController::class);


// Route::get('/searchproduct', [TransaksiController::class, 'search_product']);
// Route::get('/search','TransaksiController@search');

Route::get('/cart', function () {
    $cart = session('cart');
dd($cart);
});
Route::get('/cart/tambah/{id}', [TransaksiController::class, 'tambah_cart'])->where('id', '[0-9]+');
Route::get('/cart/hapus/{id}', [TransaksiController::class, 'hapus_cart'])->where('id', '[0-9]+');
Route::post('/cart/ubah/quantity/{id}', [TransaksiController::class, 'edit_quantity']);




Route::get('/admin/dashboard', [DashboardController::class, 'index'])->name('dashboard');
//Route::get('/admin/kategori/index', [KategoriController::class, 'index'])->name('kategori');
//Route::get('admin/kategori/form', [DashboardController::class, 'create'])->name('kategori');
//Route::get('/admin/kategori/ukuran', [UkuranController::class, 'index'])->name('kategori');

//Route::get('/admin/kategori/listproduk', [ListprodukController::class, 'index'])->name('kategori');
//Route::get('/produk/create', [ListprodukController::class, 'create'] );
//Route::post('/produk', [ListprodukController::class, 'store'] );

//Route::get('admin/kategori/formkategori', [KategoriController::class, 'create'])->name('kategori');
//Route::get('admin/kategori/formukuran', [UkuranController::class, 'create'])->name('kategori');
//Route::get('admin/kategori/formlist', [ListprodukController::class, 'create'])->name('kategori');

